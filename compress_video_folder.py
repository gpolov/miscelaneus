#!/usr/bin/env python3

import subprocess
import os
import sys
import argparse
import time  # Import the time module
import os
import shutil

def file_size_diff_within_limit(original_path, encoded_path, limit=0.4):
    """Check if the file size difference is within the specified limit (50% by default)."""
    original_size = os.path.getsize(original_path)
    encoded_size = os.path.getsize(encoded_path)
    v = abs(original_size - encoded_size) / original_size
    print(v)
    return abs(original_size - encoded_size) / original_size > limit

def convert_videos(input_dir, codec='libx265', crf='35', preset='superfast',  separate_folders= False, check=False, dryrun=False):
    if codec not in ['libx264', 'libx265']:
        print(f"Unsupported codec: {codec}")
        return

    output_extension = 'h264.mp4' if codec == 'libx264' else 'h265.mp4'
    codec_suffix = '_H264' if codec == 'libx264' else '_H265'
    preset_abbr = {'ultrafast':'uf', 'superfast': 'sf', 'veryfast': 'vf', 'medium': 'me', 'slow': 'sl', 'slower': 'sr', 'veryslow': 'vs'}

    for root, dirs, files in os.walk(input_dir):
        for file in files:
            if file.endswith('.mp4') and not file.endswith(output_extension):
                input_path = os.path.join(root, file)
                output_file = f"{os.path.splitext(file)[0]}_crf_{crf}_preset_{preset_abbr.get(preset, 'vf')}.{output_extension}"
                if separate_folders:
                    output_path = os.path.join(root + codec_suffix, output_file)
                else:
                    output_path = os.path.join(root, output_file)

                # Check if the output file already exists 9mj
                if os.path.exists(output_path):
                    if check:
                        if file_size_diff_within_limit(input_path, output_path):
                            print(f"Skipping {output_path} as it meets the size criteria.")
                            continue
                        else:
                            print(f"★★★ WARNING: Redoing Potential BAD CONVERSION: Skipping {input_path} as converted file exists in: {output_path} ★★★")
                            os.remove(output_path)  # Remove the file before continuing
                            #print(f"Removed {output_path}. Proceeding with conversion.")
                    else:
                        print(f"Skipping already converted file: {output_path} with no file size checking")
                        continue


                print(f"Converting {input_path} to {output_path}...", flush=True)
                start_time = time.time()  # Start timing
                
                command = [
                    'ffmpeg',
                    '-i', input_path,
                    '-c:v', codec,
                    '-preset', preset,
                    '-crf', crf,
                    '-c:a', 'aac',
                    '-b:a', '128k',
                    output_path,
                    '-loglevel', 'error'
                ]

                try:
                    if not dryrun:
                        subprocess.run(command, check=True)
                        end_time = time.time()  # End timing
                        print(f"Conversion completed in {end_time - start_time:.2f} seconds.")  # Print the time taken
                except subprocess.CalledProcessError as e:
                    print(f"Failed to convert {input_path}: {e}", file=sys.stderr)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Convert videos to a specified codec with given CRF and preset values.')
    parser.add_argument('input_directory', type=str, help='Directory containing videos to convert.')
    parser.add_argument('--codec', type=str, choices=['libx264', 'libx265'], default='libx265', help='Video codec (default: libx265).')
    parser.add_argument('--crf', type=str, default='35', help='CRF value (default: 35).')
    parser.add_argument('--preset', type=str, default='superfast', help='Encoding preset (default: superfast).')
    parser.add_argument('--separate_folders', action='store_true', help='Create separate folders with extension of the coding selected')
    parser.add_argument('--check', action='store_true', help='Check if the original and encoded files do not differ in size by more than 50%.')
    parser.add_argument('--dryrun', action='store_true', help='Dry run to see what is the state of the files')

    args = parser.parse_args()

    convert_videos(args.input_directory, codec=args.codec, crf=args.crf, preset=args.preset, separate_folders=args.separate_folders, check=args.check, dryrun=args.dryrun)
