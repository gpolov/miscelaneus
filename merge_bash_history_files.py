#!/usr/bin/env python

# Scrit that given 2 bash history files, merge them into a single file.
# Besides that it also removes duplicates.

import argparse

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("file1", help="First file to merge")
    parser.add_argument("file2", help="Second file to merge")
    parser.add_argument("output", help="Output file")
    args = parser.parse_args()
    return args

def get_file_lines(file, encoding="utf-8"):
    with open(file, "r", encoding=encoding) as f:
        lines = f.readlines()
    return lines

# Get file command lines in a dict with the timestamp as key
def get_file_lines_as_dict(file):
    # Try and except to avoid errors if the encoding is not utf-8
    try:
        lines = get_file_lines(file)
    except UnicodeDecodeError:
        lines = get_file_lines(file, encoding="latin-1")
    
    lines_dict = {}
    timestamp = None
    for line in lines:
        if line.startswith("#"):
            timestamp = int(line[1:])
        elif timestamp:
            lines_dict[timestamp] = line
            timestamp = None
        else: # Ignore lines without timestamp
            continue
    return lines_dict

# Remove duplicates values from a dict returning the dict without duplicates
def remove_duplicates_from_dict(dict):
    command_set = set()
    for timestamp in sorted(dict.keys(), reverse=True):
        command = dict[timestamp]
        if command not in command_set:
            command_set.add(command)
        else:
            del dict[timestamp]
    return dict

# Merge 2 dicts into a single dict ordered by timestamp
def merge_dicts(dict1, dict2):
    merged_dict = {}
    for timestamp in sorted(dict1.keys()):
        merged_dict[timestamp] = dict1[timestamp]
    for timestamp in sorted(dict2.keys()):
        merged_dict[timestamp] = dict2[timestamp]
    return merged_dict

# Write a dict to a file from a dict with the timestamp as key
def write_dict_to_file(dict, file):
    with open(file, "w", encoding='utf-8') as f:
        for timestamp in sorted(dict.keys()):
            f.write("#{}\n".format(timestamp))
            f.write(dict[timestamp])

def main():
    args = get_args()
    file1_dict = get_file_lines_as_dict(args.file1)
    file2_dict = get_file_lines_as_dict(args.file2)
    merged_dict = merge_dicts(file1_dict, file2_dict)
    merged_dict = remove_duplicates_from_dict(merged_dict)
    write_dict_to_file(merged_dict, args.output)

if __name__ == "__main__":
    main()


