#!/bin/bash

# Check if directory path is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 directory_path"
    exit 1
fi

# Get the directory from the command line argument
directory=$1

# Check if the directory exists
if [ ! -d "$directory" ]; then
    echo "Directory $directory does not exist."
    exit 1
fi

# Initialize a counter for total words
total_words=0

# Loop over all text files in the directory
for file in "$directory"/*; do
    # Only process files (not directories)
    if [ -f "$file" ]; then
        # Count the words in the file and add to the total
        word_count=$(wc -w < "$file")
        total_words=$((total_words + word_count))
    fi
done

# Print the total word count
echo "Total words in all files in directory $directory: $total_words"

