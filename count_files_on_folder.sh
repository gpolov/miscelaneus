#!/bin/bash

# Check if directory path is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 directory_path"
    exit 1
fi

# Get the directory from the command line argument
directory=$1

# Check if the directory exists
if [ ! -d "$directory" ]; then
    echo "Directory $directory does not exist."
    exit 1
fi

# Count the number of files in the directory and its subdirectories
file_count=$(find "$directory" -type f | wc -l)

echo "Total number of files in directory $directory (including subdirectories): $file_count"

